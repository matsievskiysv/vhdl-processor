#!/usr/bin/env bash

while read f
do
sed -i -E \
    -e 's|\[dumpfile\] *"/.+?/([^/]+.ghw)"|\[dumpfile\] "\1"|g' \
    -e 's|\[savefile\] *"/.+?/([^/]+.gtkw)"|\[savefile\] "\1"|g' $f
done <<< $(find . -type f -iname '*.gtkw')
