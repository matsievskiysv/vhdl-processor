#include "assembler.h"

#include "lexer.yy.h"

#include <stdio.h>
#include <stdlib.h>

static const char *file_name;
static FILE	  *outfile;

enum SECTION current_section = TEXT;

bool section_present[3] = {false, false, false};

void
yyerror(char const *message)
{
	fprintf(stderr, "%s (%s:%d): %s\n", message, file_name, yyget_lineno(), yyget_text());
}

void
insert_newline(void)
{
	fprintf(outfile, "\n");
}

void
__insert_code(uint16_t code)
{
	for (int i = 15; i >= 0; --i) {
		fprintf(outfile, "%u", !!(code & (1 << i)));
	}
}

void
insert_section(enum SECTION section)
{
	current_section = section;
	char *section_name;
	switch (section) {
	case TEXT:
		section_name = ".text";
		break;
	case DATA:
		section_name = ".data";
		break;
	case BSS:
		section_name = ".bss";
		break;
	}
	if (section_present[section]) {
		yyerror("Section already defined");
		exit(1);
	}
	section_present[section] = true;
	fprintf(outfile, "%s", section_name);
}

void
insert_label(char *label)
{
	if (!section_present[TEXT] && !section_present[DATA] && !section_present[BSS]) {
		yyerror("Cannot put label outside of section");
		exit(1);
	}
	fprintf(outfile, "%s:", label);
}

void
insert_label_ref(char *label, bool is_relative, unsigned int from_mask, unsigned int to_mask)
{
	if (!section_present[TEXT] && !section_present[DATA] && !section_present[BSS]) {
		yyerror("Cannot put label outside of section");
		exit(1);
	}
	fprintf(outfile, "<%s:%04x:%04x:%s>", is_relative ? "r" : "a", from_mask, to_mask, label);
}

void
insert_code(uint16_t code)
{
	if ((!section_present[TEXT]) || (current_section != TEXT)) {
		yyerror("Cannot put code outside of section .text");
		exit(1);
	}
	__insert_code(code);
}

void
insert_word(int value)
{
	int max_val = 1 << 16;
	if ((!section_present[DATA]) || (current_section != DATA)) {
		yyerror("Cannot put constant outside of section .data");
		exit(1);
	}
	if ((value > (max_val / 2 - 1)) || (value < (-max_val / 2))) {
		yyerror("Value out of range");
		exit(1);
	}
	__insert_code((uint16_t) value);
}

void
insert_space(unsigned int count)
{
	if ((!section_present[BSS]) || (current_section != BSS)) {
		yyerror("Cannot put constant outside of section .bss");
		exit(1);
	}
	for (unsigned int i = 0; i < count; ++i) {
		__insert_code(0);
		insert_newline();
	}
}

uint8_t
to_immediate(int value, unsigned int bit_count, bool is_unsigned)
{
	int max_val = 1 << bit_count;
	uint8_t out_value = (uint8_t) (value & (max_val - 1));
	if (is_unsigned == true) {
		if (value < 0) {
			yyerror("Negative out_value for unsigned operation");
			exit(1);
		}
		if (value > (max_val - 1)) {
			yyerror("Value out of range");
			exit(1);
		}
	} else {
		if ((value > (max_val / 2 - 1)) || (value < (-max_val / 2))) {
			yyerror("Value out of range");
			exit(1);
		}
	}
	return out_value;
}

int
main(int argc, char const *argv[])
{
	if (argc != 3) {
		fprintf(stderr, "Usage: ./assembler <intput> <output>\n");
		return 1;
	}
	file_name	= argv[1];
	if( access(file_name, F_OK ) == -1 ) {
		fprintf(stderr, "File %s does not exist\n", file_name);
		return 1;
	}
	yyin		= fopen(file_name, "r");
	outfile		= fopen(argv[2], "w");
	int result_code = yyparse();
	fclose(yyin);
	if (!section_present[TEXT]) {
		insert_section(TEXT);
		insert_newline();
	}
	if (!section_present[DATA]) {
		insert_section(DATA);
		insert_newline();
	}
	if (!section_present[BSS]) {
		insert_section(BSS);
		insert_newline();
	}
	fclose(outfile);
	yylex_destroy();
	return result_code;
}
