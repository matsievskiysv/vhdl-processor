%option noyywrap

%top{
        #include <stdlib.h>
        #include "parser.tab.h"
        #include "assembler.h"
        extern void yyerror(char const *message);
}

%s EXPR_START

REG r[0-7]
REGSEP ,
LBLSEP :
LBL ([a-zA-Z]+[a-zA-Z0-9]*)
IMM_HEX 0x[0-9a-f]+
IMM_OCT 0o[0-9a-f]+
IMM_DEC [0-9a-f]+
COMMENT [#@].*$

%%

".text"			{ yylval.val=TEXT;return SEC; }
".data"			{ yylval.val=DATA;return SEC; }
".bss"			{ yylval.val=BSS;return SEC; }

"add"|"ADD"/[ \t]	{ yylval.val=0;yylval.code.code = ADD;yylval.code.usgn = false;return CMD_RRR; }
"add.u"|"ADD.U"/[ \t]	{ yylval.val=0;yylval.code.code = ADD;yylval.code.usgn = true;return CMD_RRR; }
"addi"|"ADDI"/[ \t]	{ yylval.val=0;yylval.code.code = ADDI;yylval.code.usgn = false;return CMD_RRI; }
"addi.u"|"ADDI.U"/[ \t]	{ yylval.val=0;yylval.code.code = ADDI;yylval.code.usgn = true;return CMD_RRI; }

"and"|"AND"/[ \t]	{ yylval.val=0;yylval.code.code = AND;return CMD_RRR; }
"andi"|"ANDI"/[ \t]	{ yylval.val=0;yylval.code.code = ANDI;return CMD_RRI; }

"or"|"OR"/[ \t]		{ yylval.val=0;yylval.code.code = OR;return CMD_RRR; }
"ori"|"ORI"/[ \t]	{ yylval.val=0;yylval.code.code = ORI;return CMD_RRI; }

"xor"|"XOR"/[ \t]	{ yylval.val=0;yylval.code.code = XOR;return CMD_RRR; }
"xori"|"XORI"/[ \t]	{ yylval.val=0;yylval.code.code = XORI;return CMD_RRI; }

"shl"|"SHL"/[ \t]	{ yylval.val=0;yylval.code.code = SHL;return CMD_RRR; }
"shli"|"SHLI"/[ \t]	{ yylval.val=0;yylval.code.code = SHLI;return CMD_RRI; }

"shr"|"SHR"/[ \t]	{ yylval.val=0;yylval.code.code = SHR;return CMD_RRR; }
"shri"|"SHRI"/[ \t]	{ yylval.val=0;yylval.code.code = SHRI;return CMD_RRI; }

"sha"|"SHA"/[ \t]	{ yylval.val=0;yylval.code.code = SHA;return CMD_RRR; }
"shai"|"SHAI"/[ \t]	{ yylval.val=0;yylval.code.code = SHAI;return CMD_RRI; }

"not"|"NOT"/[ \t]	{ yylval.val=0;yylval.code.code = NOT;return CMD_RR; }

"cmp.eq"|"CMP.EQ"/[ \t]		{ yylval.val=0;yylval.code.code = CMP_EQ;yylval.code.usgn = false;return CMD_RRR; }
"cmp.u.eq"|"CMP.U.EQ"/[ \t]	{ yylval.val=0;yylval.code.code = CMP_EQ;yylval.code.usgn = true;return CMD_RRR; }
"cmpi.eq"|"CMPI.EQ"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_EQ;yylval.code.usgn = false;return CMD_RRI; }
"cmpi.u.eq"|"CMPI.U.EQ"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_EQ;yylval.code.usgn = true;return CMD_RRI; }

"cmp.lt"|"CMP.LT"/[ \t]		{ yylval.val=0;yylval.code.code = CMP_LT;yylval.code.usgn = false;return CMD_RRR; }
"cmp.u.lt"|"CMP.U.LT"/[ \t]	{ yylval.val=0;yylval.code.code = CMP_LT;yylval.code.usgn = true;return CMD_RRR; }
"cmpi.lt"|"CMPI.LT"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_LT;yylval.code.usgn = false;return CMD_RRI; }
"cmpi.u.lt"|"CMPI.U.LT"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_LT;yylval.code.usgn = true;return CMD_RRI; }

"cmp.gt"|"CMP.GT"/[ \t]		{ yylval.val=0;yylval.code.code = CMP_GT;yylval.code.usgn = false;return CMD_RRR; }
"cmp.u.gt"|"CMP.U.GT"/[ \t]	{ yylval.val=0;yylval.code.code = CMP_GT;yylval.code.usgn = true;return CMD_RRR; }
"cmpi.gt"|"CMPI.GT"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_GT;yylval.code.usgn = false;return CMD_RRI; }
"cmpi.u.gt"|"CMPI.U.GT"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_GT;yylval.code.usgn = true;return CMD_RRI; }

"bi"|"BI"/[ \t]			{ yylval.val=0;yylval.code.code = BI;return CMD_RI; }

"jl"|"JL"/[ \t]			{ yylval.val=0;yylval.code.code = JL;return CMD_RI; }

"ret"|"RET"/[ \t]		{ yylval.val=0;yylval.code.code = RET;return CMD_R; }

"ld"|"LD"/[ \t]			{ yylval.val=0;yylval.code.code = LD;return CMD_RR; }

"str"|"STR"/[ \t]		{ yylval.val=0;yylval.code.code = STR;return CMD_RR; }

"lb"|"LB"/[ \t]			{ yylval.val=0;yylval.code.code = LB;return CMD_RI; }

"cmp.ne"|"CMP.NE"/[ \t]		{ yylval.val=0;yylval.code.code = CMP_NE;yylval.code.usgn = false;return M_RRR; }
"cmp.u.ne"|"CMP.U.NE"/[ \t]	{ yylval.val=0;yylval.code.code = CMP_NE;yylval.code.usgn = true;return M_RRR; }
"cmpi.ne"|"CMPI.NE"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_NE;yylval.code.usgn = false;return M_RRI; }
"cmpi.u.ne"|"CMPI.U.NE"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_NE;yylval.code.usgn = true;return M_RRI; }

"cmp.le"|"CMP.LE"/[ \t]		{ yylval.val=0;yylval.code.code = CMP_LE;yylval.code.usgn = false;return M_RRR; }
"cmp.u.le"|"CMP.U.LE"/[ \t]	{ yylval.val=0;yylval.code.code = CMP_LE;yylval.code.usgn = true;return M_RRR; }
"cmpi.le"|"CMPI.LE"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_LE;yylval.code.usgn = false;return M_RRI; }
"cmpi.u.le"|"CMPI.U.LE"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_LE;yylval.code.usgn = true;return M_RRI; }

"cmp.ge"|"CMP.GE"/[ \t]		{ yylval.val=0;yylval.code.code = CMP_GE;yylval.code.usgn = false;return M_RRR; }
"cmp.u.ge"|"CMP.U.GE"/[ \t]	{ yylval.val=0;yylval.code.code = CMP_GE;yylval.code.usgn = true;return M_RRR; }
"cmpi.ge"|"CMPI.GE"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_GE;yylval.code.usgn = false;return M_RRI; }
"cmpi.u.ge"|"CMPI.U.GE"/[ \t]	{ yylval.val=0;yylval.code.code = CMPI_GE;yylval.code.usgn = true;return M_RRI; }

"notl"|"NOTL"/[ \t]		{ yylval.val=0;yylval.code.code = NOTL;return M_RR; }

"nop"|"NOP"			{ yylval.val=0;yylval.code.code = NOP;return M_Z; }

"mov"|"MOV"/[ \t]		{ yylval.val=0;yylval.code.code = MOV;return M_RR; }

"jll"|"JLL"/[ \t]		{ yylval.val=0;yylval.code.code = JLL;return M_RL; }

"bl"|"BL"/[ \t]			{ yylval.val=0;yylval.code.code = BL;return M_RL; }

"b"|"B"/[ \t]			{ yylval.val=0;yylval.code.code = B;return M_L; }

"la"|"LA"/[ \t]			{ yylval.val=0;yylval.code.code = LA;return M_RL; }

".word"		{ yylval.val=WORD;return INST; }

".space"	{ yylval.val=SPACE;return INST; }

"zero"		{ yylval.val = 0;return REG; }

{REG}		{ yylval.val = yytext[1]-'0';return REG; }

{REGSEP}	{ return REGSEP; }

<EXPR_START>{LBL}	{ yylval.str = strdup(yytext);return LBL; }

{IMM_HEX}	{ yylval.val = strtoul(yytext+2, NULL, 16);return IMM; }

-{IMM_HEX}	{ yylval.val = -strtoul(yytext+3, NULL, 16);return IMM; }

{IMM_OCT}	{ yylval.val = strtoul(yytext+2, NULL, 8);return IMM; }

-{IMM_OCT}	{ yylval.val = -strtoul(yytext+3, NULL, 8);return IMM; }

-?{IMM_DEC}	{ yylval.val = strtoul(yytext, NULL, 10);return IMM; }

{LBLSEP}	{ return LBLSEP; }

\n		{ yylineno++;BEGIN(EXPR_START);return CMDSEP; }

;+		{ BEGIN(EXPR_START);return CMDSEP; }

[" "\t]		{}

{COMMENT}	{}

.|\n		{ yyerror("Unexpected input");exit(1); }
