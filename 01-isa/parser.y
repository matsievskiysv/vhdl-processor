%{
	#include <stdlib.h>
	extern int yyerror(char *message);
	extern int yylex(void);
%}

%code requires {
	#include "assembler.h"
}

%union {
	code_token_t code;
	int val;
	char* str;
}

%token <code> CMD_RRR CMD_RRI CMD_RR CMD_RI CMD_R CMD_I
%token <code> M_RRR M_RRI M_RR M_RL M_L M_Z
%token <val> CMDSEP REG REGSEP LBLSEP IMM SEC INST
%token <str> LBL

%%

input:	input expr CMDSEP
	| input CMDSEP
	| %empty;

expr:	section
	| label unlabled_expr
	| label CMDSEP unlabled_expr
	| unlabled_expr;

section: SEC {
	insert_section(yylval.val);
	insert_newline();
}

label: LBL[lbl] LBLSEP {
	insert_label($lbl);
	free($lbl);
};

unlabled_expr:	instruction
		| command_rrr
		| command_rri
		| command_rr
		| command_ri
		| command_r
		| command_macro;

instruction: INST[cmd] IMM[imm] {
	if ($cmd == WORD) {
		insert_word($imm);
		insert_newline();
	} else if ($cmd == SPACE) {
		insert_space($imm);
	} else {
		yyerror("Unknown instruction");
		exit(1);
	}
}

command_macro:	macro_rrr
		| macro_rri
		| macro_rr
		| macro_rl
		| macro_l
		| macro_z;

command_rrr: CMD_RRR[cmd] REG[rd] REGSEP REG[r1] REGSEP REG[r2] {
	command_rrr_t code = {
		.bits = {
			.code = $cmd.code,
			.rd = $rd,
			.r1 = $r1,
			.r2 = $r2,
			.usgn = $cmd.usgn
		}
	};
	insert_code(code.val);
	insert_newline();
};

command_rri: CMD_RRI[cmd] REG[rd] REGSEP REG[r1] REGSEP IMM[val] {
	uint8_t value = to_immediate($val, 4, $cmd.usgn);
	command_rri_t code = {
		.bits = {
			.code = $cmd.code,
			.rd = $rd,
			.r1 = $r1,
			.imm_val = value,
			.usgn = $cmd.usgn
		}
	};
	insert_code(code.val);
	insert_newline();
};

command_rr: CMD_RR[cmd] REG[rd] REGSEP REG[r1] {
	command_rr_t code = {
		.bits = {
			.code = $cmd.code,
			.rd = $rd,
			.r1 = $r1
		}
	};
	insert_code(code.val);
	insert_newline();
};

command_ri: CMD_RI[cmd] REG[rd] REGSEP IMM[val] {
	uint8_t value = to_immediate($val, 8, $cmd.usgn);
	command_ri_t code = {
		.bits = {
			.code = $cmd.code,
			.rd = $rd,
			.imm_val = value
		}
	};
	insert_code(code.val);
	insert_newline();
};

command_r: CMD_R[cmd] REG[rd] {
	command_r_t code = {
		.bits = {
			.code = $cmd.code,
			.rd = $rd
		}
	};
	insert_code(code.val);
	insert_newline();
};

macro_rrr: M_RRR[cmd] REG[rd] REGSEP REG[r1] REGSEP REG[r2] {
	{
		command_rrr_t code = {
			.bits = {
				.code = CMP_EQ,
				.rd = $rd,
				.r1 = $r1,
				.r2 = $r2,
				.usgn = $cmd.usgn
			}
		};
		switch ($cmd.code) {
			case CMP_NE:
				code.bits.code = CMP_EQ;
				break;
			case CMP_LE:
				code.bits.code = CMP_GT;
				break;
			case CMP_GE:
				code.bits.code = CMP_LT;
				break;
			default:
				yyerror("Unknown macro");
				exit(1);
				break;
		}
		insert_code(code.val);
		insert_newline();
	}
	command_rri_t code = {
		.bits = {
			.code = CMPI_EQ,
			.rd = $rd,
			.r1 = $rd,
			.imm_val = 0,
			.usgn = true
		}
	};
	insert_code(code.val);
	insert_newline();
};

macro_rri: M_RRI[cmd] REG[rd] REGSEP REG[r1] REGSEP IMM[val] {
	{
		uint8_t value = to_immediate($val, 4, $cmd.usgn);
		command_rri_t code = {
			.bits = {
				.code = CMPI_EQ,
				.rd = $rd,
				.r1 = $r1,
				.imm_val = value,
				.usgn = $cmd.usgn
			}
		};
		switch ($cmd.code) {
			case CMPI_NE:
				code.bits.code = CMPI_EQ;
				break;
			case CMPI_LE:
				code.bits.code = CMPI_GT;
				break;
			case CMPI_GE:
				code.bits.code = CMPI_LT;
				break;
			default:
				yyerror("Unknown macro");
				exit(1);
				break;
		}
		insert_code(code.val);
		insert_newline();
	}
	command_rri_t code = {
		.bits = {
			.code = CMPI_EQ,
			.rd = $rd,
			.r1 = $rd,
			.imm_val = 0,
			.usgn = true
		}
	};
	insert_code(code.val);
	insert_newline();
};

macro_rr: M_RR[cmd] REG[rd] REGSEP REG[r1] {
	command_rri_t code = {
		.bits = {
			.code = ADDI,
			.rd = $rd,
			.r1 = $r1,
			.imm_val = 0,
			.usgn = true
		}
	};
	switch ($cmd.code) {
		case NOTL:
			code.bits.code = CMPI_EQ;
			insert_code(code.val);
			insert_newline();
			break;
		case MOV:
			code.bits.code = ADDI;
			insert_code(code.val);
			insert_newline();
			break;
		default:
			yyerror("Unknown macro");
			exit(1);
	}
};

macro_rl: M_RL[cmd] REG[rd] REGSEP LBL[lbl] {
	uint8_t value = to_immediate(0, 8, false);
	switch ($cmd.code) {
		case BL: {
			command_ri_t code = {
				.bits = {
					.code = BI,
					.rd = 0,
					.imm_val = value
				}
			};
			insert_code(code.val);
			insert_label_ref($lbl, true, 0xff, 0xff00);
			insert_newline();
			free($lbl);
			break;
		}
		case JLL: {
			command_ri_t code = {
				.bits = {
					.code = JL,
					.rd = $rd,
					.imm_val = value
				}
			};
			insert_code(code.val);
			insert_label_ref($lbl, true, 0xff, 0xff00);
			insert_newline();
			free($lbl);
			break;
		}
		case LA: {
			command_ri_t code_load = {
				.bits = {
					.code = LB,
					.rd = $rd,
					.imm_val = value
				}
			};
			command_rri_t code_shift = {
				.bits = {
					.code = SHLI,
					.rd = $rd,
					.r1 = $rd,
					.imm_val = 4,
					.usgn = false
				}
			};
			insert_code(code_load.val);
			insert_label_ref($lbl, false, 0xff00, 0xff00);
			insert_newline();
			insert_code(code_shift.val);
			insert_newline();
			insert_code(code_load.val);
			insert_label_ref($lbl, false, 0xff, 0xff00);
			insert_newline();
			free($lbl);
			break;
		}
		default:
			yyerror("Unknown macro");
			exit(1);
			break;
	}
};

macro_l: M_L[cmd] LBL[lbl] {
	switch ($cmd.code) {
		case B:
			uint8_t value = to_immediate(0, 8, false);
			command_ri_t code = {
				.bits = {
					.code = JL,
					.rd = 0,
					.imm_val = value
				}
			};
			insert_code(code.val);
			insert_label_ref($lbl, true, 0xff, 0xff00);
			insert_newline();
			free($lbl);
			break;
		default:
			yyerror("Unknown macro");
			exit(1);
			break;
	}
};

macro_z: M_Z[cmd] {
	switch ($cmd.code) {
		case NOP:
			command_rrr_t code = {
				.bits = {
					.code = ADD,
					.rd = 0,
					.r1 = 0,
					.r2 = 0,
					.usgn = false
				}
			};
			insert_code(code.val);
			insert_newline();
			break;
		default:
			yyerror("Unknown macro");
			exit(1);
			break;
	}
};
