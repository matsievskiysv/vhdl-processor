.data
max:	.word 5

.bss
arr:	.space 5

# r1 - max value
# r2 - accumulator
# r3 - address
# r4 - link register
.text
	la r1, max
	ld r1, r1
	mov r2, r0
	jll r4, sum
	la r3, arr
	str r3, r2
	b wait
	nop

# r5 - counter
# r6 - compare result
sum:
	mov r5, r0
loop:
	add r2, r2, r5
	addi.u r5, r5, 1
	cmp.u.eq r6, r5, r1
	bl r6, loop
	nop
	ret r1
	nop

wait:
	b wait
	nop
