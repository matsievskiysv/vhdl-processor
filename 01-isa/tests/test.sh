#!/usr/bin/env bash

for s in $(find . -type f -iname "test_*.s"); do
    echo test $s
    valgrind -q ../assembler ${s} ${s%.s}.o || exit 1
    diff -q ${s%.s}.o ${s%.s}.o.ref || exit 1
    ../linker ${s%.s}.o ${s%.s}.bin || exit 1
    diff -q ${s%.s}.bin ${s%.s}.bin.ref || exit 1
done

echo Success!
