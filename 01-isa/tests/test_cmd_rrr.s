.text
add:	add r7, r7, r7
	add.u r7, r7, r7
not:	not r7, r7
and:	and r7, r7, r7
or:	or r7, r7, r7
xor:	xor r7, r7, r7
shl:	shl r7, r7, r7
shr:	shr r7, r7, r7
sha:	sha r7, r7, r7
cmpeq:	cmp.eq r7, r7, r7
	cmp.u.eq r7, r7, r7
cmplt:	cmp.lt r7, r7, r7
	cmp.u.lt r7, r7, r7
cmpgt:	cmp.gt r7, r7, r7
	cmp.u.gt r7, r7, r7
