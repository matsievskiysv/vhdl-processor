.text
d1:	addi r0, r0, 0
	addi r0, r0, 1
	addi r0, r0, 2
	addi r0, r0, 3
	addi r0, r0, 4
	addi r0, r0, 5
	addi r0, r0, 6
	addi r0, r0, 7
d2:	addi r0, r0, -1
	addi r0, r0, -2
	addi r0, r0, -3
	addi r0, r0, -4
	addi r0, r0, -5
	addi r0, r0, -6
	addi r0, r0, -7
	addi r0, r0, -8
x1:	addi r0, r0, 0x0
	addi r0, r0, 0x1
	addi r0, r0, 0x2
	addi r0, r0, 0x3
	addi r0, r0, 0x4
	addi r0, r0, 0x5
	addi r0, r0, 0x6
	addi r0, r0, 0x7
x2:	addi r0, r0, -0x1
	addi r0, r0, -0x2
	addi r0, r0, -0x3
	addi r0, r0, -0x4
	addi r0, r0, -0x5
	addi r0, r0, -0x6
	addi r0, r0, -0x7
	addi r0, r0, -0x8
o1:	addi r0, r0, 0o0
	addi r0, r0, 0o1
	addi r0, r0, 0o2
	addi r0, r0, 0o3
	addi r0, r0, 0o4
	addi r0, r0, 0o5
	addi r0, r0, 0o6
	addi r0, r0, 0o7
o2:	addi r0, r0, -0o1
	addi r0, r0, -0o2
	addi r0, r0, -0o3
	addi r0, r0, -0o4
	addi r0, r0, -0o5
	addi r0, r0, -0o6
	addi r0, r0, -0o7
	addi r0, r0, -0o10
d3:	addi.u r0, r0, 0
	addi.u r0, r0, 1
	addi.u r0, r0, 2
	addi.u r0, r0, 3
	addi.u r0, r0, 4
	addi.u r0, r0, 5
	addi.u r0, r0, 6
	addi.u r0, r0, 7
	addi.u r0, r0, 8
	addi.u r0, r0, 9
	addi.u r0, r0, 10
	addi.u r0, r0, 11
	addi.u r0, r0, 12
	addi.u r0, r0, 13
	addi.u r0, r0, 14
	addi.u r0, r0, 15
x3:	addi.u r0, r0, 0x0
	addi.u r0, r0, 0x1
	addi.u r0, r0, 0x2
	addi.u r0, r0, 0x3
	addi.u r0, r0, 0x4
	addi.u r0, r0, 0x5
	addi.u r0, r0, 0x6
	addi.u r0, r0, 0x7
	addi.u r0, r0, 0x8
	addi.u r0, r0, 0x9
	addi.u r0, r0, 0xa
	addi.u r0, r0, 0xb
	addi.u r0, r0, 0xc
	addi.u r0, r0, 0xd
	addi.u r0, r0, 0xe
	addi.u r0, r0, 0xf
o3:	addi.u r0, r0, 0o0
	addi.u r0, r0, 0o1
	addi.u r0, r0, 0o2
	addi.u r0, r0, 0o3
	addi.u r0, r0, 0o4
	addi.u r0, r0, 0o5
	addi.u r0, r0, 0o6
	addi.u r0, r0, 0o7
	addi.u r0, r0, 0o10
	addi.u r0, r0, 0o11
	addi.u r0, r0, 0o12
	addi.u r0, r0, 0o13
	addi.u r0, r0, 0o14
	addi.u r0, r0, 0o15
	addi.u r0, r0, 0o16
	addi.u r0, r0, 0o17
