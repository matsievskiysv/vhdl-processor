.text
add:	addi r7, r7, -1
	addi.u r7, r7, 15
and:	andi r7, r7, -1
or:	ori r7, r7, -1
xor:	xori r7, r7, -1
shl:	shli r7, r7, -1
shr:	shri r7, r7, -1
sha:	shai r7, r7, -1
cmpeq:	cmpi.eq r7, r7, -1
	cmpi.u.eq r7, r7, 15
cmplt:	cmpi.lt r7, r7, -1
	cmpi.u.lt r7, r7, 15
cmpgt:	cmpi.gt r7, r7, -1
	cmpi.u.gt r7, r7, 15
