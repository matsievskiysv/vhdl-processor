#pragma once

#include <stdbool.h>
#include <stdint.h>

enum OPCODE {
	ADD	= 0b00000,
	ADDI	= 0b00001,
	NOT	= 0b00010,
	AND	= 0b00100,
	ANDI	= 0b00101,
	OR	= 0b00110,
	ORI	= 0b00111,
	XOR	= 0b01000,
	XORI	= 0b01001,
	SHL	= 0b01010,
	SHLI	= 0b01011,
	SHR	= 0b01100,
	SHRI	= 0b01101,
	SHA	= 0b01110,
	SHAI	= 0b01111,
	CMP_EQ	= 0b10000,
	CMPI_EQ = 0b10001,
	CMP_LT	= 0b10010,
	CMPI_LT = 0b10011,
	CMP_GT	= 0b10100,
	CMPI_GT = 0b10101,
	LD	= 0b11000,
	STR	= 0b11001,
	LB	= 0b11010,
	JL	= 0b11011,
	RET	= 0b11100,
	BI	= 0b11101,
};

enum MACRO {
	CMP_NE,
	CMPI_NE,
	CMP_LE,
	CMPI_LE,
	CMP_GE,
	CMPI_GE,
	NOTL,
	NOP,
	MOV,
	BL,
	B,
	JLL,
	LA,
};

enum INSTRUCTION {
	WORD,
	SPACE,
};

enum SECTION { TEXT, DATA, BSS };

typedef struct {
	uint8_t code : 5;
	bool	usgn : 1;
} code_token_t;

typedef union {
	struct {
		uint8_t code : 5;
		uint8_t rd   : 3;
		uint8_t r1   : 3;
		uint8_t r2   : 3;
		bool	     : 1;
		bool usgn    : 1;
	} bits;
	uint16_t val;
} command_rrr_t;

typedef union {
	struct {
		uint8_t code	: 5;
		uint8_t rd	: 3;
		uint8_t r1	: 3;
		uint8_t imm_val : 4;
		bool	usgn	: 1;
	} bits;
	uint16_t val;
} command_rri_t;

typedef union {
	struct {
		uint8_t code : 5;
		uint8_t rd   : 3;
		uint8_t r1   : 3;
		uint8_t	     : 5;
	} bits;
	uint16_t val;
} command_rr_t;

typedef union {
	struct {
		uint8_t code	: 5;
		uint8_t rd	: 3;
		uint8_t imm_val : 8;
	} bits;
	uint16_t val;
} command_ri_t;

typedef union {
	struct {
		uint8_t code : 5;
		uint8_t rd   : 3;
		uint8_t	     : 8;
	} bits;
	uint16_t val;
} command_r_t;

void insert_newline(void);

void insert_section(enum SECTION section);

void insert_label(char *label);

void insert_label_ref(char *label, bool is_relative, unsigned int from_mask, unsigned int to_mask);

void insert_code(uint16_t code);

void insert_word(int value);

void insert_space(unsigned int count);

uint8_t to_immediate(int value, unsigned int bit_count, bool is_unsigned);
