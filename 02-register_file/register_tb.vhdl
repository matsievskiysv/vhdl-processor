library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;

entity register_tb is

end entity register_tb;

architecture rtl of register_tb is

  constant TICK : time := 1 us;

  signal reg_rd1  : reg_file_t := 0;
  signal reg_rd2  : reg_file_t := 0;
  signal reg_wr   : reg_file_t := 0;
  signal data_rd1 : reg_t      := (others => '0');
  signal data_rd2 : reg_t      := (others => '0');
  signal data_wr  : reg_t      := (others => '0');
  signal wr_en    : std_ulogic := '0';
  signal rst      : std_ulogic := '1';
  signal en       : std_ulogic := '1';

  signal clk : std_ulogic := '0';

begin  -- architecture rtl

  clk1 : component clock
    port map (
      clk => clk);

  rf1 : component register_file
    port map (
      reg_rd1  => reg_rd1,
      reg_rd2  => reg_rd2,
      reg_wr   => reg_wr,
      data_rd1 => data_rd1,
      data_rd2 => data_rd2,
      data_wr  => data_wr,
      en       => en,
      clk      => clk,
      rst      => rst,
      wr_en    => wr_en);


  process is
  begin  -- process

    en      <= '1';
    rst     <= '1';
    wr_en   <= '1';
    reg_wr  <= 0;
    reg_rd1 <= 0;
    reg_rd2 <= 0;
    data_wr <= std_ulogic_vector(to_unsigned(16#4321#, reg_t'length));

    wait for TICK / 2;
    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 1;
    reg_rd1 <= 0;
    reg_rd2 <= 1;
    data_wr <= std_ulogic_vector(to_unsigned(16#5a10#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a10#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a10" severity failure;

    reg_wr  <= 2;
    reg_rd1 <= 1;
    reg_rd2 <= 2;
    data_wr <= std_ulogic_vector(to_unsigned(16#5a20#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a10#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x5a10" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a10#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a10" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a20#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a20" severity failure;

    reg_wr  <= 3;
    reg_rd1 <= 2;
    reg_rd2 <= 3;
    data_wr <= std_ulogic_vector(to_unsigned(16#5a30#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a20#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x5a20" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a20#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a20" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a30#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a30" severity failure;

    reg_wr  <= 4;
    reg_rd1 <= 3;
    reg_rd2 <= 4;
    data_wr <= std_ulogic_vector(to_unsigned(16#5a40#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a30#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x5a30" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a30#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a30" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a40#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a40" severity failure;

    reg_wr  <= 5;
    reg_rd1 <= 4;
    reg_rd2 <= 5;
    data_wr <= std_ulogic_vector(to_unsigned(16#5a50#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a40#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a40" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a40#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a40" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a50#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a50" severity failure;

    reg_wr  <= 6;
    reg_rd1 <= 5;
    reg_rd2 <= 6;
    data_wr <= std_ulogic_vector(to_unsigned(16#5a60#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a50#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a50" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a50#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a50" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a60#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a60" severity failure;

    reg_wr  <= 7;
    reg_rd1 <= 6;
    reg_rd2 <= 7;
    data_wr <= std_ulogic_vector(to_unsigned(16#5a70#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a60#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a60" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a60#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & " != " & "0x5a60" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a70#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a70" severity failure;

    wr_en   <= '0';
    reg_wr  <= 0;
    reg_rd1 <= 0;
    reg_rd2 <= 0;
    data_wr <= std_ulogic_vector(to_unsigned(16#1234#, reg_t'length));

    wait for 2 * TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 1;
    reg_rd1 <= 0;
    reg_rd2 <= 1;
    data_wr <= std_ulogic_vector(to_unsigned(16#1234#, reg_t'length));

    wait for 2 * TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a10#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a10" severity failure;

    reg_wr  <= 3;
    reg_rd1 <= 2;
    reg_rd2 <= 3;
    data_wr <= std_ulogic_vector(to_unsigned(16#1234#, reg_t'length));

    wait for 2 * TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a20#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x5a20" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a30#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a30" severity failure;

    reg_wr  <= 5;
    reg_rd1 <= 4;
    reg_rd2 <= 5;
    data_wr <= std_ulogic_vector(to_unsigned(16#1234#, reg_t'length));

    wait for 2 * TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a40#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x5a40" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a50#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a50" severity failure;

    reg_wr  <= 7;
    reg_rd1 <= 6;
    reg_rd2 <= 7;
    data_wr <= std_ulogic_vector(to_unsigned(16#1234#, reg_t'length));

    wait for 2 * TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#5a60#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x5a60" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#5a70#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x5a70" severity failure;

    rst <= '0';

    wait for TICK;

    rst     <= '1';
    wr_en   <= '1';
    reg_wr  <= 1;
    reg_rd1 <= 0;
    reg_rd2 <= 1;
    data_wr <= std_ulogic_vector(to_unsigned(16#0100#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 2;
    reg_rd1 <= 1;
    reg_rd2 <= 2;
    data_wr <= std_ulogic_vector(to_unsigned(16#0200#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0100#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0100" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 3;
    reg_rd1 <= 2;
    reg_rd2 <= 3;
    data_wr <= std_ulogic_vector(to_unsigned(16#0300#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0200#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0200" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 4;
    reg_rd1 <= 3;
    reg_rd2 <= 4;
    data_wr <= std_ulogic_vector(to_unsigned(16#0400#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0300#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0300" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 5;
    reg_rd1 <= 4;
    reg_rd2 <= 5;
    data_wr <= std_ulogic_vector(to_unsigned(16#0500#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0400#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0400" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 6;
    reg_rd1 <= 5;
    reg_rd2 <= 6;
    data_wr <= std_ulogic_vector(to_unsigned(16#0600#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0500#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0500" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    reg_wr  <= 7;
    reg_rd1 <= 6;
    reg_rd2 <= 7;
    data_wr <= std_ulogic_vector(to_unsigned(16#0700#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0600#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0600" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    en <= '0';

    wait for TICK;

    reg_wr  <= 1;
    reg_rd1 <= 2;
    reg_rd2 <= 3;
    data_wr <= std_ulogic_vector(to_unsigned(16#0200#, reg_t'length));

    wait for TICK;

    assert data_rd1 = std_ulogic_vector(to_unsigned(16#0600#, reg_t'length))
      report "rd1: " & to_hstring(data_rd1) & "!=" & "0x0600" severity failure;
    assert data_rd2 = std_ulogic_vector(to_unsigned(16#0#, reg_t'length))
      report "rd2: " & to_hstring(data_rd2) & " != " & "0x0" severity failure;

    wait for TICK;

    rst <= '0';

    wait for TICK;

  end process;

end architecture rtl;
