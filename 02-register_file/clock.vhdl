library ieee;
use ieee.std_logic_1164.all;

use work.config.all;

entity clock is

  port (clk : out std_ulogic);

end entity clock;

architecture rtl of clock is

  signal clk_int : std_ulogic := '0';

begin  -- architecture rtl

  clk <= clk_int;

  process is
  begin  -- process
    wait for TICK;
    clk_int <= not clk_int;
  end process;

end architecture rtl;
