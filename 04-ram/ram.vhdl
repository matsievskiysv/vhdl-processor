library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;

entity ram is

  generic (
    RAM_BASE : address_t;
    RAM_SIZE : address_t);

  port (
    clk      : in  std_ulogic;
    en       : in  std_ulogic;
    wr_en    : in  std_ulogic;
    address  : in  address_t;
    data_in  : in  reg_t;
    data_out : out reg_t);

end entity ram;

architecture rtl of ram is
  type mem_t is array (0 to RAM_SIZE-1) of reg_t;
  signal mem : mem_t := (others => (others => '0'));
begin  -- architecture rtl

  ram1 : process (clk) is
    variable address_real : address_t;
  begin  -- process ram1
    if clk'event and clk = '1' then     -- rising clock edge
      if en = '1' then
        if address >= RAM_BASE and address < RAM_BASE + RAM_SIZE - 1 then
          address_real := address - RAM_BASE;
          if wr_en = '1' then
            mem(address_real) <= data_in;
          else
            data_out <= mem(address_real);
          end if;
        end if;
      end if;
    end if;
  end process ram1;

end architecture rtl;
