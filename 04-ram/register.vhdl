library ieee;
use ieee.std_logic_1164.all;

use work.config.all;

entity register_file is

  port (reg_rd1, reg_rd2, reg_wr : in  reg_file_t;
        data_rd1, data_rd2       : out reg_t;
        data_wr                  : in  reg_t;
        clk                      : in  std_ulogic;
        en                       : in  std_ulogic;
        rst                      : in  std_ulogic;
        wr_en                    : in  std_ulogic);

end entity register_file;

architecture rtl of register_file is

  type mem_t is array (1 to reg_count-1) of reg_t;

  signal mem : mem_t := (others => (others => '0'));

begin  -- architecture rtl

  process (clk, rst) is
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      mem      <= (others => (others => '0'));
      -- write_back <= (others => '0');
      data_rd1 <= (others => '0');
      data_rd2 <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if en = '1' then
        if reg_rd1 = 0 then
          data_rd1 <= (others => '0');
        else
          data_rd1 <= mem(reg_rd1);
        end if;
        if reg_rd2 = 0 then
          data_rd2 <= (others => '0');
        else
          data_rd2 <= mem(reg_rd2);
        end if;
        if wr_en = '1' and reg_wr > 0 then
          mem(reg_wr) <= data_wr;
        end if;
      end if;
    end if;
  end process;

end architecture rtl;
