library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;

entity ram_tb is

end entity ram_tb;

architecture rtl of ram_tb is

  signal address           : address_t  := 0;
  signal data_in, data_out : reg_t      := (others => '0');
  signal clk               : std_ulogic;
  signal en                : std_ulogic := '1';
  signal wr_en             : std_ulogic := '0';

begin  -- architecture rtl

  clock1 : component clock
    port map (
      clk => clk);

  ram1 : component ram
    generic map (
      RAM_BASE => MMAP_RAM_BASE,
      RAM_SIZE => MMAP_RAM_SIZE)
    port map (
      clk      => clk,
      en       => en,
      wr_en    => wr_en,
      address  => address,
      data_in  => data_in,
      data_out => data_out);

  process is
  begin  -- process
    en      <= '1';
    address <= 20;
    wr_en   <= '1';
    data_in <= std_ulogic_vector(to_unsigned(16#4321#, reg_t'length));

    wait for TICK / 2;
    wait for 2*TICK;

    wr_en <= '0';

    wait for 2*TICK;

    assert data_out = std_ulogic_vector(to_unsigned(16#4321#, reg_t'length))
      report "data_out: " & to_hstring(data_out) & "!=" & "0x4321" severity failure;

    address <= 21;
    wr_en   <= '1';
    data_in <= std_ulogic_vector(to_unsigned(16#4021#, reg_t'length));

    wait for 2*TICK;

    wr_en <= '0';

    wait for 2*TICK;

    assert data_out = std_ulogic_vector(to_unsigned(16#4021#, reg_t'length))
      report "data_out: " & to_hstring(data_out) & "!=" & "0x4021" severity failure;

    address <= 20;
    wr_en   <= '1';
    data_in <= std_ulogic_vector(to_unsigned(16#4001#, reg_t'length));

    wait for 2*TICK;

    wr_en <= '0';

    wait for 2*TICK;

    assert data_out = std_ulogic_vector(to_unsigned(16#4001#, reg_t'length))
      report "data_out: " & to_hstring(data_out) & "!=" & "0x4001" severity failure;

    address <= 2;
    wr_en   <= '1';
    data_in <= std_ulogic_vector(to_unsigned(16#4001#, reg_t'length));

    wait for 2*TICK;

    wr_en <= '0';

    wait for 2*TICK;

    assert data_out = std_ulogic_vector(to_unsigned(16#4001#, reg_t'length))
      report "data_out: " & to_hstring(data_out) & "!=" & "0x4001" severity failure;

    wait for 2*TICK;

    en      <= '0';
    address <= 20;
    wr_en   <= '0';

    wait for 2*TICK;

    assert data_out = std_ulogic_vector(to_unsigned(16#4001#, reg_t'length))
      report "data_out: " & to_hstring(data_out) & "!=" & "0x4001" severity failure;

    en      <= '1';

    wait for 2*TICK;

    assert data_out = std_ulogic_vector(to_unsigned(16#4001#, reg_t'length))
      report "data_out: " & to_hstring(data_out) & "!=" & "0x4001" severity failure;

  end process;

end architecture rtl;
