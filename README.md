# Лабораторные работы по VHDL

## Лабораторные работы

* [Логические операции](01-bit_logic/README.md)
* [Условные операции](02-multiplexor/README.md)
* [Сумматор](03-full_adder/README.md)
* [Сдвиговый регистр](04-shift-register/README.md)
* [Буфер FIFO](05-fifo/README.md)
* [Конфигурация](06-configuration/README.md)
* [ROM память](07-rom/README.md)

## Установка необходимых программ

Настройка переменных.

```bash
export INSTALL_DIR=/opt
export BUILD_DIR=/tmp/build
mkdir -p $BUILD_DIR
sudo mkdir -p $INSTALL_DIR
```

### `gtkwave`

Установка `gtkwave` из репозитория:

```bash
sudo apt install gtkwave
```

### `ghdl` и `ghdl-ls`

Сборка `ghdl` VHDL компилятора и VHDL LSP сервера.

```bash
cd $BUILD_DIR
sudo apt build-dep ghdl
git clone https://github.com/ghdl/ghdl.git
pushd ghdl
./configure --prefix=/opt/ghdl
make -j $(nproc)
sudo make install
pip3 install --user .
echo 'export PATH=$PATH':"$INSTALL_DIR/ghdl/bin" >> $HOME/.profile
popd
```
