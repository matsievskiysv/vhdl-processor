library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

use work.config.all;

entity rom is

  generic (
    ROM_FILE : string;
    ROM_BASE : address_t;
    ROM_SIZE : address_t);

  port (
    clk     : in  std_ulogic;
    address : in  address_t;
    data    : out reg_t);

end entity rom;

architecture rtl of rom is
  type mem_t is array (0 to ROM_SIZE-1) of reg_t;
  signal mem : mem_t := (others => (others => '0'));
begin  -- architecture rtl

  rom1 : process (clk) is
    variable address_real : address_t;
  begin  -- process rom1
    if clk'event and clk = '1' then     -- rising clock edge
      if address >= ROM_BASE and address < ROM_BASE + ROM_SIZE - 1 then
        address_real := address - ROM_BASE;
        data         <= mem(address_real);
      end if;
    end if;
  end process rom1;

  process
    variable fstatus : file_open_status;
    file fptr        : text;

    variable file_line         : line;
    variable data_internal_var : reg_t;
  begin

    file_open(fstatus, fptr, ROM_FILE, read_mode);

    for item in 0 to ROM_SIZE-1 loop
      if not endfile(fptr) then
        readline(fptr, file_line);
        bread(file_line, data_internal_var);
        mem(item) <= data_internal_var;
      else
        mem(item) <= (others => '0');
      end if;
    end loop;

    file_close(fptr);
    wait;
  end process;

end architecture rtl;
