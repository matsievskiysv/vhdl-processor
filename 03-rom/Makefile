GHDL := ghdl
STD ?= 08
GHDL_OPTS ?= -Wall -Werror
WORK ?= work
TOP_UNIT ?= rom_tb
WAVE = $(TOP_UNIT).ghw
SYM_TIME ?= 80us

VHDL_SOURCE_FILES := $(filter-out %_tb.vhdl,$(wildcard *.vhdl))
VHDL_TEST_FILES := $(wildcard *_tb.vhdl)
OPTIONS = --work=$(WORK) --std=$(STD) $(GHDL_OPTS)
RUN_OPTIONS = --wave=$(WAVE) --stop-time=$(SYM_TIME)

.DEFAULT_GOAL = view

$(WORK)-obj$(STD).cf: $(VHDL_SOURCE_FILES) $(VHDL_TEST_FILES)
	$(GHDL) import $(OPTIONS) $^

.PHONY: import
import: $(WORK)-obj$(STD).cf

.PHONY: check
check: $(WORK)-obj$(STD).cf
	$(foreach f, $(VHDL_SOURCE_FILES) $(VHDL_TEST_FILES), $(GHDL) syntax $(OPTIONS) $(f);)

.analyze: $(WORK)-obj$(STD).cf
	$(GHDL) make $(OPTIONS) $(TOP_UNIT)
	touch .analyze

.PHONY: analyze
analyze: .analyze

data.bin:
	./generate_data.sh

$(WAVE): .analyze data.bin
	$(GHDL) run $(OPTIONS) $(TOP_UNIT) $(RUN_OPTIONS)

.PHONY: simulate
simulate: $(WAVE)

.PHONY: view
view: $(WAVE)
	gtkwave wave.gtkw

.PHONY: clean
clean:
	$(GHDL) --remove $(OPTIONS)
	rm -f $(WORK)-obj$(STD).cf
	rm -f .analyze
	rm -f $(WAVE)
	rm -f $(UNIT).zip
	find . \( \
	-iname "*.cf" \
	-o -iname "*.ghw" \
	-o -iname "*.json" \
	-o -iname "*.bin" \
	\) -delete

.PHONY: zip
zip:
	zip --symlinks --recurse-paths $(UNIT).zip .
