library ieee;
use ieee.std_logic_1164.all;

package config is

  constant FREQ      : natural := 1000000;
  constant TICK      : time    := 1 sec / freq / 2;
  constant WORD_SIZE : natural := 16;
  constant REG_COUNT : natural := 8;

  subtype reg_t is std_ulogic_vector(WORD_SIZE-1 downto 0);
  subtype reg_file_t is natural range 0 to (REG_COUNT-1);
  subtype address_t is natural range 0 to (2**WORD_SIZE)-1;

  constant ROM_FILE      : string    := "data.bin";
  constant MMAP_ROM_BASE : address_t := 16#5#;
  constant MMAP_ROM_SIZE : address_t := 16#100#;


  component clock is

    port (clk : out std_ulogic);

  end component clock;


  component register_file is

    port (reg_rd1, reg_rd2, reg_wr : in  reg_file_t;
          data_rd1, data_rd2       : out reg_t;
          data_wr                  : in  reg_t;
          en                       : in  std_ulogic;
          clk                      : in  std_ulogic;
          rst                      : in  std_ulogic;
          wr_en                    : in  std_ulogic);

  end component register_file;


  component rom is

    generic (
      ROM_FILE : string;
      ROM_BASE : address_t;
      ROM_SIZE : address_t);

    port (
      clk     : in  std_ulogic;
      address : in  address_t;
      data    : out reg_t);

  end component rom;

end package config;
