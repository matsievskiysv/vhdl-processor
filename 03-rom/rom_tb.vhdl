library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;

entity rom_tb is

end entity rom_tb;

architecture rtl of rom_tb is

  signal address         : address_t := 0;
  signal data            : reg_t     := (others => '0');
  signal clk             : std_ulogic;

begin  -- architecture rtl

  clock1 : component clock
    port map (
      clk => clk);

  rom1 : component rom
    generic map (
      ROM_FILE => ROM_FILE,
      ROM_BASE => MMAP_ROM_BASE,
      ROM_SIZE => MMAP_ROM_SIZE)
    port map (
      clk     => clk,
      address => address,
      data    => data);

  process (clk) is
  begin  -- process
    if clk'event and clk = '0' then  -- rising clock edge
      address <= address + 1;
    end if;
  end process;

end architecture rtl;
