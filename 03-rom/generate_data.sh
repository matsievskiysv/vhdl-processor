#!/usr/bin/env bash

FILE=data.bin

rm -f $FILE
for i in $(seq 64); do
    for j in $(seq 16); do
	printf '%d' $((RANDOM % 2)) >> $FILE
    done
    printf '\n' >> $FILE
done
