.text
regs:
	add zero, r0, r0
	add r0, r0, r0
	add r3, r0, r0
	add r7, r0, r0
	add r0, r3, r0
	add r0, r7, r0
	add r0, r0, r3
	add r0, r0, r7
	add r2, r5, r3
add:	add r7, r7, r7
	add.u r7, r7, r7
not:	not r7, r7
and:	and r7, r7, r7
or:	or r7, r7, r7
xor:	xor r7, r7, r7
shl:	shl r7, r7, r7
shr:	shr r7, r7, r7
sha:	sha r7, r7, r7
cmpeq:	cmp.eq r7, r7, r7
	cmp.u.eq r7, r7, r7
cmplt:	cmp.lt r7, r7, r7
	cmp.u.lt r7, r7, r7
cmpgt:	cmp.gt r7, r7, r7
	cmp.u.gt r7, r7, r7
addi:	addi r7, r7, -1
	addi.u r7, r7, 15
andi:	andi r7, r7, -1
ori:	ori r7, r7, -1
xori:	xori r7, r7, -1
shli:	shli r7, r7, -1
shri:	shri r7, r7, -1
shai:	shai r7, r7, -1
cmpieq:	cmpi.eq r7, r7, -1
	cmpi.u.eq r7, r7, 15
cmpilt:	cmpi.lt r7, r7, -1
	cmpi.u.lt r7, r7, 15
cmpigt:	cmpi.gt r7, r7, -1
	cmpi.u.gt r7, r7, 15
ld:	ld r0, r0
	ld r4, r3
	ld r7, r7
str:	str r0, r0
	str r4, r3
	str r7, r7
lb:	lb r0, 0
	lb r7, -1
jl:	jl r0, 0
	jl r7, -1
bi:	bi r0, 0
	bi r7, -1
ret:	ret r0
	ret r7
