library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;

entity decoder_tb is

end entity decoder_tb;

architecture rtl of decoder_tb is

  constant TICK               : time      := 1 us;
  signal pc                   : address_t := address_t'high;
  signal code                 : reg_t;
  signal clk_rom, clk_decoder : std_ulogic;
  signal decoded              : decoded_t;
  signal err, en              : std_ulogic;

begin  -- architecture rtl

  rom1 : component rom
    generic map (
      ROM_FILE => ROM_FILE,
      ROM_BASE => MMAP_ROM_BASE,
      ROM_SIZE => MMAP_ROM_SIZE)
    port map (
      clk     => clk_rom,
      address => pc,
      data    => code);

  decoder1 : component decoder
    port map (
      clk     => clk_decoder,
      en      => en,
      code    => code,
      decoded => decoded,
      err     => err);

  process is
  begin  -- process
    -- REG
    pc          <= 0;
    en          <= '1';
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 1;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 2;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 3
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "3" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 3;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 4;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 3
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "3" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 5;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 6;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 3
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "3" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 7;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 8;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 2
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "2" severity failure;
    assert decoded.reg1 = 5
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "5" severity failure;
    assert decoded.reg2 = 3
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "3" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- ADD
    pc          <= 9;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 10;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 11;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_NOT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_NOT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 0
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- AND
    pc          <= 12;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_AND
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- OR
    pc          <= 13;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_OR
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_OR" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- XOR
    pc          <= 14;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_XOR
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_XOR" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- SHL
    pc          <= 15;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_SHL
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_SHL" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- SHR
    pc          <= 16;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_SHR
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_SHR" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- SHA
    pc          <= 17;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_SHA
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_SHA" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- CMP.EQ
    pc          <= 18;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_EQ
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_EQ" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 19;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_EQ
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_EQ" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- CMP.LT
    pc          <= 20;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_LT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_LT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 21;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_LT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_LT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- CMP.GT
    pc          <= 22;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_GT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_GT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 23;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_GT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_GT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- ADDI
    pc          <= 24;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADDI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADDI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 25;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADDI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADDI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- ANDI
    pc          <= 26;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ANDI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ANDI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- ORI
    pc          <= 27;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ORI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ORI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- XORI
    pc          <= 28;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_XORI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_XORI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- SHLI
    pc          <= 29;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_SHLI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_SHLI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- SHRI
    pc          <= 30;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_SHRI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_SHRI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- SHAI
    pc          <= 31;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_SHAI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_SHAI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- CMP.EQ
    pc          <= 32;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_EQ
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_EQ" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 33;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_EQ
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_EQ" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- CMP.LT
    pc          <= 34;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_LT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_LT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 35;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_LT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_LT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- CMP.GT
    pc          <= 36;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_GT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_GT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = false
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "false" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 37;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_CMP_GT
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_CMP_GT" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#f#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xf" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- LD
    pc          <= 38;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_LD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_LD" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 39;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_LD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_LD" severity failure;
    assert decoded.regd = 4
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "4" severity failure;
    assert decoded.reg1 = 3
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "3" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 40;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_LD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_LD" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- STR
    pc          <= 41;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_STR
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_STR" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 42;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_STR
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_STR" severity failure;
    assert decoded.regd = 4
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "4" severity failure;
    assert decoded.reg1 = 3
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "3" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    pc          <= 43;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_STR
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_STR" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

    -- LB
    pc          <= 44;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_LB
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_LB" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 0
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    pc          <= 45;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_LB
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_LB" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#ff#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xff" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    -- JL
    pc          <= 46;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_JL
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_JL" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    pc          <= 47;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_JL
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_JL" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#ff#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xff" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    -- BI
    pc          <= 48;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_BI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_BI" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    pc          <= 49;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_BI
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_BI" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = true
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "true" severity failure;
    assert decoded.imm_val = 16#ff#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0xff" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    -- RET
    pc          <= 50;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_RET
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_RET" severity failure;
    assert decoded.regd = 0
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "0" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    pc          <= 51;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_RET
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_RET" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    en          <= '0';
    pc          <= 10;
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_RET
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_RET" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 0
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "0" severity failure;
    assert decoded.reg2 = 0
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "0" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = true
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "true" severity failure;
    wait for TICK;

    en          <= '1';
    clk_rom     <= '0';
    clk_decoder <= '0';
    wait for TICK;
    clk_rom     <= '1';
    wait for TICK;
    clk_decoder <= '1';
    wait for TICK;
    assert decoded.cmd = CMD_ADD
      report "decoded.cmd: " & to_hstring(code) & " " & to_string(decoded.cmd) & "!=" & "CMD_ADD" severity failure;
    assert decoded.regd = 7
      report "decoded.regd: " & to_hstring(code) & " " & to_string(decoded.regd) & "!=" & "7" severity failure;
    assert decoded.reg1 = 7
      report "decoded.reg1: " & to_hstring(code) & " " & to_string(decoded.reg1) & "!=" & "7" severity failure;
    assert decoded.reg2 = 7
      report "decoded.reg2: " & to_hstring(code) & " " & to_string(decoded.reg2) & "!=" & "7" severity failure;
    assert decoded.imm = false
      report "decoded.imm: " & to_hstring(code) & " " & to_string(decoded.imm) & "!=" & "false" severity failure;
    assert decoded.imm_val = 16#0#
      report "decoded.imm_val: " & to_hstring(code) & " " & to_string(decoded.imm_val) & "!=" & "0x0" severity failure;
    assert decoded.usgnd = true
      report "decoded.usgnd: " & to_hstring(code) & " " & to_string(decoded.usgnd) & "!=" & "true" severity failure;
    assert decoded.branch = false
      report "decoded.branch: " & to_hstring(code) & " " & to_string(decoded.branch) & "!=" & "false" severity failure;
    wait for TICK;

  end process;

end architecture rtl;
