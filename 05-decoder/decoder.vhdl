library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.config.all;

entity decoder is

  port (
    clk     : in  std_ulogic;
    en      : in  std_ulogic;
    code    : in  reg_t;
    decoded : out decoded_t;
    err     : out std_ulogic);

end entity decoder;

architecture rtl of decoder is

  subtype cmd_slice_t is std_ulogic_vector(CMD_SIZE-1 downto 0);
  subtype reg_slice_t is std_ulogic_vector(REG_SIZE-1 downto 0);
  subtype imm_big_slice_t is std_ulogic_vector(IMM_BIG_SIZE-1 downto 0);
  subtype imm_sml_slice_t is std_ulogic_vector(IMM_SML_SIZE-1 downto 0);

  signal cmd_slice                          : cmd_slice_t;
  signal reg1_slice, reg2_slice, regd_slice : reg_slice_t;
  signal imm_big_slice                      : imm_big_slice_t;
  signal imm_sml_slice                      : imm_sml_slice_t;
  signal usgnd                              : boolean;

begin  -- architecture rtl

  cmd_slice     <= code(4 downto 0);
  regd_slice    <= code(7 downto 5);
  reg1_slice    <= code(10 downto 8);
  reg2_slice    <= code(13 downto 11);
  usgnd         <= code(15) = '1';
  imm_sml_slice <= code(14 downto 11);
  imm_big_slice <= code(15 downto 8);

  process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      if en = '1'
      then
        err <= '0';
        case cmd_slice is
          when "00000" =>
            decoded.cmd     <= CMD_ADD;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "00001" =>
            decoded.cmd     <= CMD_ADDI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "00010" =>
            decoded.cmd     <= CMD_NOT;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= true;
            decoded.branch  <= false;
          when "00100" =>
            decoded.cmd     <= CMD_AND;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "00101" =>
            decoded.cmd     <= CMD_ANDI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "00110" =>
            decoded.cmd     <= CMD_OR;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "00111" =>
            decoded.cmd     <= CMD_ORI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01000" =>
            decoded.cmd     <= CMD_XOR;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01001" =>
            decoded.cmd     <= CMD_XORI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01010" =>
            decoded.cmd     <= CMD_SHL;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01011" =>
            decoded.cmd     <= CMD_SHLI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01100" =>
            decoded.cmd     <= CMD_SHR;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01101" =>
            decoded.cmd     <= CMD_SHRI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01110" =>
            decoded.cmd     <= CMD_SHA;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "01111" =>
            decoded.cmd     <= CMD_SHAI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "10000" =>
            decoded.cmd     <= CMD_CMP_EQ;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "10001" =>
            decoded.cmd     <= CMD_CMP_EQ;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "10010" =>
            decoded.cmd     <= CMD_CMP_LT;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "10011" =>
            decoded.cmd     <= CMD_CMP_LT;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "10100" =>
            decoded.cmd     <= CMD_CMP_GT;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(to_integer(unsigned(reg2_slice)));
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "10101" =>
            decoded.cmd     <= CMD_CMP_GT;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_sml_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= usgnd;
            decoded.branch  <= false;
          when "11000" =>
            decoded.cmd     <= CMD_LD;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= true;
            decoded.branch  <= false;
          when "11001" =>
            decoded.cmd     <= CMD_STR;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(to_integer(unsigned(reg1_slice)));
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= true;
            decoded.branch  <= false;
          when "11010" =>
            decoded.cmd     <= CMD_LB;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(0);
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_big_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= true;
            decoded.branch  <= true;
          when "11011" =>
            decoded.cmd     <= CMD_JL;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(0);
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_big_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= true;
            decoded.branch  <= true;
          when "11101" =>
            decoded.cmd     <= CMD_BI;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(0);
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= to_integer(unsigned(imm_big_slice));
            decoded.imm     <= true;
            decoded.usgnd   <= true;
            decoded.branch  <= true;
          when "11100" =>
            decoded.cmd     <= CMD_RET;
            decoded.regd    <= reg_file_t(to_integer(unsigned(regd_slice)));
            decoded.reg1    <= reg_file_t(0);
            decoded.reg2    <= reg_file_t(0);
            decoded.imm_val <= 0;
            decoded.imm     <= false;
            decoded.usgnd   <= true;
            decoded.branch  <= true;
          when others => err <= '1';
        end case;
      end if;
    end if;
  end process;

end architecture rtl;
