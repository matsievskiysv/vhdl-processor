library ieee;
use ieee.std_logic_1164.all;

package config is

  constant FREQ         : natural := 1000000;
  constant TICK         : time    := 1 sec / freq / 2;
  constant WORD_SIZE    : natural := 16;
  constant REG_COUNT    : natural := 8;
  constant CMD_SIZE     : natural := 5;
  constant REG_SIZE     : natural := 3;
  constant IMM_BIG_SIZE : natural := 8;
  constant IMM_SML_SIZE : natural := 4;

  subtype reg_t is std_ulogic_vector(WORD_SIZE-1 downto 0);
  subtype reg_file_t is natural range 0 to (REG_COUNT-1);
  subtype address_t is natural range 0 to (2**WORD_SIZE)-1;
  subtype imm_big_t is natural range 0 to (2**IMM_BIG_SIZE)-1;
  subtype imm_sml_t is natural range 0 to (2**IMM_SML_SIZE)-1;

  constant ROM_FILE      : string    := "test_code.bin";
  constant MMAP_ROM_BASE : address_t := 16#0#;
  constant MMAP_ROM_SIZE : address_t := 16#100#;
  constant MMAP_RAM_BASE : address_t := 16#5#;
  constant MMAP_RAM_SIZE : address_t := 16#100#;

  type cmd_t is
    (CMD_ADD,
     CMD_ADDI,
     CMD_NOT,
     CMD_AND,
     CMD_ANDI,
     CMD_OR,
     CMD_ORI,
     CMD_XOR,
     CMD_XORI,
     CMD_SHL,
     CMD_SHLI,
     CMD_SHR,
     CMD_SHRI,
     CMD_SHA,
     CMD_SHAI,
     CMD_CMP_EQ,
     CMD_CMPI_EQ,
     CMD_CMP_LT,
     CMD_CMPI_LT,
     CMD_CMP_GT,
     CMD_CMPI_GT,
     CMD_LD,
     CMD_STR,
     CMD_LB,
     CMD_JL,
     CMD_BI,
     CMD_RET);

  type decoded_t is record
    cmd              : cmd_t;
    reg1, reg2, regd : reg_file_t;
    imm_val          : imm_big_t;
    imm              : boolean;
    usgnd            : boolean;
    branch           : boolean;
  end record decoded_t;


  component clock is

    port (clk : out std_ulogic);

  end component clock;


  component register_file is

    port (reg_rd1, reg_rd2, reg_wr : in  reg_file_t;
          data_rd1, data_rd2       : out reg_t;
          data_wr                  : in  reg_t;
          en                       : in  std_ulogic;
          clk                      : in  std_ulogic;
          rst                      : in  std_ulogic;
          wr_en                    : in  std_ulogic);

  end component register_file;


  component rom is

    generic (
      ROM_FILE : string;
      ROM_BASE : address_t;
      ROM_SIZE : address_t);

    port (
      clk     : in  std_ulogic;
      address : in  address_t;
      data    : out reg_t);

  end component rom;


  component ram is

    generic (
      RAM_BASE : address_t;
      RAM_SIZE : address_t);

    port (
      clk      : in  std_ulogic;
      en       : in  std_ulogic;
      wr_en    : in  std_ulogic;
      address  : in  address_t;
      data_in  : in  reg_t;
      data_out : out reg_t);

  end component ram;


  component decoder is

    port (
      clk     : in  std_ulogic;
      en      : in  std_ulogic;
      code    : in  reg_t;
      decoded : out decoded_t;
      err     : out std_ulogic);

  end component decoder;

end package config;
